const restful = require('node-restful')
const mongoose = restful.mongoose
const bet = require('../bet/bet')

const userSchema = new mongoose.Schema({
    name: {type: String, required: true},
    idade: {type: Number, min:18, max: 125, required: true},
    sexo: {type: String, required: true, enum:['M', 'F']},
    cpf: {type: String},
    bets: [bet]
})

module.exports = restful.model('user', userSchema)
