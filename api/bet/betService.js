const bet = require('./bet')
const _ = require('lodash')

bet.methods(['get', 'post', 'put', 'delete'])
bet.updateOptions({new: true, runValidators: true})

//uso do noderestful para interceptar os metodos acima para que chame um middleware de tratamento de erros
bet.after('post', sendErrorsOrNext).after('put', sendErrorsOrNext)

function sendErrorsOrNext(res, res, next){
    const bundle = res.locals.bundle
    if(bundle.errors){
        var errors = parseErrors(bundle.errors)
        res.status(500).json({errors})
    }else{
        next()
    }
}

function parseErrors(nodeRestfulErrors){
    const errors = []
    _.forIn(nodeRestfulErrors, error => errors.push(error.message))
    return errors
}

bet.route('count', function(req, res, next){
    bet.count(function(error, value){
        if(error){
            res.status(500).json({errors: [error]})
        }else{
            res.json({value})
        }
    })
})


module.exports = bet 