const restful = require('node-restful')
const mongoose = restful.mongoose

const betSchema = new mongoose.Schema({
    name: {type: String, required: true},
    value: {type: Number, min: 0, required: [true, 'Informe o valor do prêmio.']},
    moeda: {type: String, required: true},
    dtInicio: {type: Date},
    dtFim: {type: Date},
    dtUpdated: {type: Date, default: Date.now()},
    descricao:{type: String},
    status: {type: String, required: true, uppercase: true,
     enum: ['ATIVO', 'INATIVO', 'VENCIDO', 'CANCELADO']}
})

module.exports = restful.model('bet', betSchema)