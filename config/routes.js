const express = require('express')

module.exports = function(server){
    
    //especifica todos os midlewares q serão chamados a partir de /api
    const router = express.Router()
    server.use('/api', router)

    //pega o arquivo do serviço
    const betService = require('../api/bet/betService')
    /*
    register é metodo do noterestful para registrar os verbos http
    por isso devemos incluir o modulo
    */
    betService.register(router, '/bets')


    const userService = require('../api/user/userService')
    userService.register(router, '/users')
}